/*
 * SyngoMRProtocol.h
 *
 *      Author: kvahed
 *      Project: codeare
 */

#ifndef _SYNGO_MR_PROTOCOL_H_
#define _SYNGO_MR_PROTOCOL_H_

#include <boost/property_tree/ptree.hpp>
#include <boost/regex.hpp>

#include <string>
#include <fstream>
#include <iostream>

#if (__cplusplus >= 201103L)
#define STOI(x) std::stoi(x)
#define STOF(x) std::stof(x)
#else
#define STOI(x) atoi(x.c_str())
#define STOF(x) atof(x.c_str())
#endif

/**
 * @brief Type conversion from string
 */
template <typename T> struct ConvertTo;

template<> struct ConvertTo<short> {
	static short str (const std::string& in) { return STOI(in); }
};
template<> struct ConvertTo<int> {
	static int str (const std::string& in) { return STOI(in); }
};
template<> struct ConvertTo<long> {
	static long str (const std::string& in) { return STOI(in); }
};
template<> struct ConvertTo<unsigned short> {
	static unsigned short str (const std::string& in) { return STOI(in); }
};
template<> struct ConvertTo<unsigned int> {
	static unsigned int str (const std::string& in) { return STOI(in); }
};
template<> struct ConvertTo<unsigned long> {
	static unsigned long str (const std::string& in) { return STOI(in); }
};
template<> struct ConvertTo<float> {
	static float str (const std::string& in) { return STOF(in); }
};
template<> struct ConvertTo<double> {
	static double str (const std::string& in) { return STOF(in); }
};
template<> struct ConvertTo<const char*> {
	static const char* str (const std::string& in) { return in.c_str(); } 
};
template<> struct ConvertTo<std::string> {
	static std::string str (const std::string& in) { return std::string(in); } 
};

/**
 * @brief Search result
 **/
typedef struct search_result {
	long _pos;
	std::string _str;

	/**
	 * @brief Construct with position and found string
	 * @param pos position
	 * @param str found string
	 */
	search_result (long pos = LONG_MAX, const std::string& str = "") :
		_pos(pos), _str(str) {};

	/**
	 * @brief Get position
	 * @return position in search text
	 */
	inline long position () const { return _pos;}

	/**
	 * @brief Get found string
	 * @return found string
	 */
	inline const std::string& str () const { return _str; }

	/**
	 * @brief Get length of found string
	 * @return length of found string
	 */
	inline long length () const { return _str.length(); }

	/**
	 * @brief Empty?
	 * @return empty search?
	 */
	inline bool empty() const { return _str.empty(); }

	/**
	 * @brief Found?
	 * @return found anything?
	 */
	inline bool found() const { return !_str.empty(); }

	/**
	 * @brief Dump found string to output stream
	 * @param output stream to dump to
	 * @param the search result
	 * @return the output stream to dump to
	 */
	friend std::ostream& operator<< (std::ostream& os,
			const search_result& res) {	os << res.str(); return os;	}

} search_result;

/**
 * @brief Protocol representation Syngo MR
 */
class SyngoMRProtocol {

public:

	typedef boost::property_tree::ptree stack_item;

	enum read_exception {
		CANNOT_OPEN = 100,
		ZERO_HEADER_LENGTH,
		TOO_LARGE_HEADER
	};

	enum parse_exception {
		FAILED_TO_FIND_XPROTOCOL = 200,
		HIT_EOF_BEFORE_FINISHING,
		PROTOCOL_HIERARCHY_ERROR
	};

	enum get_exception {
		INVALID_PATH = 300,
		CONVERSION_FAILED
	};

	enum out_type {
		PLAIN, XML, JSON, INI
	};

	enum major_version {VAB, VD};

	/**
	 * @default Constructor
	 */
	SyngoMRProtocol();

	/**
	 * @brief Construct with file name
	 */
	SyngoMRProtocol (const std::string& filename, int verbosity = 0, bool remove_empty_tags = true);

	/**
	 * @brief Copy
	 */
	SyngoMRProtocol (const SyngoMRProtocol& protocol);

	/**
	 * @brief Clean up
	 */
	virtual ~SyngoMRProtocol();

	/**
	 * @brief Property object
	 */
	virtual const boost::property_tree::ptree& Properties () const;

	/**
	 * @brief Get file name
	 */
	virtual const std::string& FileName () const;

	/**
	 * @brief Raw output
	 */
	virtual const std::string& Raw () const;

	/**
	 * @brief XML output
	 */
	virtual std::ostream& ToXML (std::ostream& os) const;
	virtual void ToXML (const std::string& filename) const;

	/**
	 * @brief Get a value from property tree
	 */
	const std::string GetStr (const std::string& path) const;

	template<typename T> T Get (const std::string& path) const {
		T ret = 0;
		std::string str_rep;
		try {
			str_rep = GetStr(path);
            ret = ConvertTo<T>::str(str_rep);
		} catch (const boost::property_tree::ptree_bad_path&) {
			printf ("  ERROR: Invalid path %s !", path.c_str());
			throw INVALID_PATH;
		} catch (const std::logic_error& e ) {
		} catch (const std::exception&) {
			printf ("  ERROR: Conversion to %s failed for %s !",
					typeid(T).name(), str_rep.c_str());
			throw CONVERSION_FAILED;
		}
		return ret;
	}

	friend std::ostream& operator<< (std::ostream &out, const SyngoMRProtocol &smrp);

	/**
	 * @brief Get the offset of the last measurement in the raw file
	 */
	uint64_t GetLastMeaseOffset() const;

private:

	/**
	 * @brief Reads file into RAM buffer for parsing
	 * @param level of verbosity
	 * @return happiness
	 */
	virtual bool ReadFile (int verbosity = 0);

	/**
	 * @brief Parse RAM buffer for content
	 * @param level of verbosity
	 * @return happiness
	 */
	virtual bool Parse (int verbosity = 0);

	virtual void TrimData ();
	//virtual void GotoXProtocol (const std::string& section = std::string("Config"));
	virtual std::string GetNextLine (size_t pos = 0);

	void HandleOpenBracket (std::vector<int>& non_stack, long pos);
	void HandleCloseBracket (std::vector<stack_item*>& stack,
			std::vector<int>& non_stack, long pos);
	void HandleNode (std::vector<stack_item*>& stack, std::vector<int>& non_stack);
    void HandleAscConvEntry (stack_item& root, const std::string& ascconv_str);

	std::string _raw;  // measurement header
	std::string _meas_file_name; // measurement file name
	uint32_t _header_len;
	uint32_t _data_len;
	major_version _mver;
	std::string::const_iterator _xpos;
	boost::property_tree::ptree _props;
	std::string::const_iterator _start, _cur, _end;

	// A flag indicating if the empty tags shall be removed when doing TrimData()
	uint64_t _last_meas_offset;
	bool _remove_empty_tags;
};


#endif /* _SYNGO_MR_PROTOCOL_H_ */
