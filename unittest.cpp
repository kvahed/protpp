/*
 * unittest.cpp
 *
 *      Author: kvahed
 *		Project: codeare
 */
#include "SyngoMRProtocol.hpp"
#include <cstdlib>
#include <cmath>
#include <iostream>

template<class T> void print_entry (const SyngoMRProtocol& smprot, const std::string& key) {
    std::cout << key << " " << smprot.Get<T>(key) << std::endl;
}

int main (int argc, char* argv[]) {

	if (argc > 1) {

		std::string fname (argv[1]);

		// Read and parse protocol from file
		SyngoMRProtocol smprot (fname.c_str(), (argc > 2) ? atoi(argv[2]) : 0, false);

		// Dump text version through piping
		std::ofstream out_txt ((fname + ".txt").c_str(), std::ios::out);
		out_txt << smprot;
		out_txt.close();

		// Dump XML version
		smprot.ToXML(fname + ".xml");

        print_entry<long>(smprot, "XProtocol.ASCCONV.lTotalScanTimeSec");
        print_entry<long>(smprot, "XProtocol.ASCCONV.sKSpace.lPartitions");
        print_entry<long>(smprot, "XProtocol.ASCCONV.sKSpace.lBaseResolution");
        print_entry<float>(smprot, "XProtocol.ASCCONV.sAdjData.sAdjVolume.dThickness");
        print_entry<float>(smprot, "XProtocol.ASCCONV.sAdjData.sAdjVolume.dPhaseFOV");
		print_entry<const char*>(smprot, "XProtocol.Dicom.ParamMap.DICOM.SoftwareVersions");

	}

	return 0;

}

